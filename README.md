# mindustry-script

In-game scripts for Mindustry

Usage:
1) Open the file
2) Copy the code section
3) Select "Import from clipboard" in Mindustry


License: GNU Affero GPL version 3 or any later version.
